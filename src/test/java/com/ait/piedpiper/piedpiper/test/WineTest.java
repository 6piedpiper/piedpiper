package com.ait.piedpiper.piedpiper.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.piedpiper.piedpiper.model.Wine;

public class WineTest {
	Wine wine;
	
	@Before
	public void test() {
		wine = new Wine();
	}
	
	@Test
	public void testGetters(){
		wine.setName("PiedPiper");
		assertEquals("PiedPiper",wine.getName());
	}

}
