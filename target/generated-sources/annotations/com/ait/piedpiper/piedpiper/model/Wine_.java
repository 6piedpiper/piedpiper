package com.ait.piedpiper.piedpiper.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Wine.class)
public abstract class Wine_ {

	public static volatile SingularAttribute<Wine, String> country;
	public static volatile SingularAttribute<Wine, String> year;
	public static volatile SingularAttribute<Wine, String> name;
	public static volatile SingularAttribute<Wine, String> description;
	public static volatile SingularAttribute<Wine, Integer> id;
	public static volatile SingularAttribute<Wine, String> region;
	public static volatile SingularAttribute<Wine, String> picture;
	public static volatile SingularAttribute<Wine, String> grapes;

}

